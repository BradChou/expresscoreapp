package com.multicast.fse

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView

class RecyclerAdapter {
    class RecyclerErrorAdapter(private var dataSet: Array<String>) :
        RecyclerView.Adapter<RecyclerErrorAdapter.ViewHolder>() {

        /**
         * Provide a reference to the type of views that you are using
         * (custom ViewHolder).
         */
        inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
            val deliveryId: TextView

            init {
                // Define click listener for the ViewHolder's View.
                deliveryId = view.findViewById(R.id.txt_deliveryId)
            }
        }

        // Create new views (invoked by the layout manager)
        override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): ViewHolder {
            // Create a new view, which defines the UI of the list item
            val view = LayoutInflater.from(viewGroup.context)
                .inflate(R.layout.item_error, viewGroup, false)

            return ViewHolder(view)
        }

        // Replace the contents of a view (invoked by the layout manager)
        override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {

            // Get element from your dataset at this position and replace the
            // contents of the view with that element
            viewHolder.deliveryId.text = dataSet[position]
        }

        // Return the size of your dataset (invoked by the layout manager)
        override fun getItemCount() = dataSet.size

        fun addItem(id:String){
            dataSet = dataSet.plus(id)
            notifyDataSetChanged()
        }
    }

    class RecyclerUploadAdapter(private var dataSet: Array<String>):
        RecyclerView.Adapter<RecyclerUploadAdapter.ViewHolder>(){
        /**
         * Provide a reference to the type of views that you are using
         * (custom ViewHolder).
         */
        inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
            val deliveryId: TextView

            init {
                // Define click listener for the ViewHolder's View.
                deliveryId = view.findViewById(R.id.txt_deliveryId)
            }
        }

        // Create new views (invoked by the layout manager)
        override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): ViewHolder {
            // Create a new view, which defines the UI of the list item
            val view = LayoutInflater.from(viewGroup.context)
                .inflate(R.layout.item_upload, viewGroup, false)

            return ViewHolder(view)
        }

        // Replace the contents of a view (invoked by the layout manager)
        override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {

            // Get element from your dataset at this position and replace the
            // contents of the view with that element
            viewHolder.deliveryId.text = dataSet[position]
        }

        // Return the size of your dataset (invoked by the layout manager)
        override fun getItemCount() = dataSet.size

        fun addItem(id:String){
            if(!dataSet.contains(id)){
                dataSet = dataSet.plus(id)
                notifyDataSetChanged()
            }
        }

        fun isExist(id:String):Boolean{
            var isExist = false
            for(i in dataSet.indices){
                if(dataSet[i] == id){
                    isExist = true
                    break
                }
            }
            return isExist
        }

        fun getData():Array<String>{
            return dataSet
        }
    }
}