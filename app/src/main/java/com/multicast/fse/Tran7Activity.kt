package com.multicast.fse

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import com.multicast.fse.databinding.ActivityTran7Binding
import com.multicast.fse.util.DialogUtil
import okhttp3.Call
import okhttp3.Callback
import okhttp3.Request
import okhttp3.Response
import okio.IOException
import org.json.JSONArray
import org.json.JSONObject

class Tran7Activity : AppCompatActivity() {

    private lateinit var binding:ActivityTran7Binding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityTran7Binding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)

        getStatistics()
    }

    fun clickBack(view: View){
        this.finish()
    }

    fun getStatistics(){
        val url = App.SERVER +"/DeliveryMaster/GetStatistics"

        val request = Request.Builder()
            .url(url)
            .header("accessToken", App.accessToken)
            .header("Cookie", App.headers["Set-Cookie"]!!) //必須給第一次cookie值，後端才知道是同一個session
            .get()
            .build()

        Log.d("url",url)

        App.client.newCall(request).enqueue(object : Callback {
            override fun onFailure(call: Call, e: IOException) {
                e.printStackTrace()
            }

            override fun onResponse(call: Call, response: Response) {
                response.use {
                    if (!response.isSuccessful) {
                        DialogUtil.showDailog(this@Tran7Activity,response.body!!.string(),getString(R.string.sys_alert_title))
                    }
                    else{
                        val responseStr = response.body!!.string()
                        Log.d("getStatistics",responseStr)

                        val json = JSONObject(responseStr)

                        runOnUiThread{
                            binding.txtTran1TotalValue.text = json.getJSONObject("Data").getString("Tran1Total")
                            binding.txtTran1CountValue.text = json.getJSONObject("Data").getString("Tran1Count")
                            binding.txtTran1UndoValue.text = (json.getJSONObject("Data").getInt("Tran1Total")-json.getJSONObject("Data").getInt("Tran1Count")).toString()
                            binding.txtTran5CountValue.text = json.getJSONObject("Data").getString("Tran5Count")
                            binding.txtTran6CountValue.text = json.getJSONObject("Data").getString("Tran6Count")
                            binding.txtTran6UndoValue.text = (json.getJSONObject("Data").getInt("Tran5Count")-json.getJSONObject("Data").getInt("Tran6Count")).toString()
                            binding.txtCollectionMoneyValue.text = json.getJSONObject("Data").getString("CollectionMoney")
                            binding.txtReceiveMoneyValue.text = json.getJSONObject("Data").getString("ReceiveMoney")
                            binding.txtUncollectedValue.text = (json.getJSONObject("Data").getInt("CollectionMoney") - json.getJSONObject("Data").getInt("ReceiveMoney")).toString()
                        }
                    }
                }
            }
        })
    }
}