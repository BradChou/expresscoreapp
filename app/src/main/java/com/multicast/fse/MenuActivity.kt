package com.multicast.fse

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import androidx.core.content.ContextCompat
import com.multicast.fse.tran1.Tran1Activity
import com.multicast.fse.databinding.ActivityMenuBinding
import com.multicast.fse.tran2.Tran2Activity
import com.multicast.fse.tran3.Tran3Activity
import com.multicast.fse.tran4.Tran4Activity
import com.multicast.fse.tran5.Tran5Activity
import com.multicast.fse.tran6.Tran6Activity
import com.multicast.fse.tran8.Tran8Activity

class MenuActivity : AppCompatActivity() {


    private lateinit var binding: ActivityMenuBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMenuBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)
        binding.txtUser.text = "Hi ${App.USER_NAME}"

        for(i in 0 until App.func.length()){
            if(App.func.getJSONObject(i).getString("CAN_RUN") == "Y"){
                view.findViewWithTag<Button>(App.func.getJSONObject(i).getString("FUNC_ID")).background = ContextCompat.getDrawable(this, R.drawable.btn_background)
                view.findViewWithTag<Button>(App.func.getJSONObject(i).getString("FUNC_ID")).isClickable = true
            }
        }

    }

    fun clickBack(view: View){
        this.finish()
    }

    fun clickTran1(view: View){
        startActivity(Intent(this, Tran1Activity::class.java))
    }

    fun clickTran2(view:View){
        startActivity(Intent(this, Tran2Activity::class.java))
    }

    fun clickTran3(view:View){
        startActivity(Intent(this, Tran3Activity::class.java))
    }

    fun clickTran4(view:View){
        startActivity(Intent(this, Tran4Activity::class.java))
    }

    fun clickTran5(view:View){
        startActivity(Intent(this, Tran5Activity::class.java))
    }

    fun clickTran6(view:View){
        startActivity(Intent(this, Tran6Activity::class.java))
    }

    fun clickTran7(view:View){
        startActivity(Intent(this,Tran7Activity::class.java))
    }

    fun clickTran8(view:View){
        startActivity(Intent(this, Tran8Activity::class.java))
    }
}