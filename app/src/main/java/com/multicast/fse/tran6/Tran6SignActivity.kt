package com.multicast.fse.tran6

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import com.multicast.fse.databinding.ActivityTran6Binding
import com.multicast.fse.databinding.ActivityTran6SignBinding

class Tran6SignActivity : AppCompatActivity() {

    private lateinit var binding:ActivityTran6SignBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityTran6SignBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)

    }

    fun clickBack(view: View){
        this.finish()
    }

    fun clickClear(view: View){
        binding.signaturePad.clear()
    }

    fun clickSign(view: View){
        Tran6Activity.signPhoto = binding.signaturePad.signatureBitmap
        this.finish()
    }

}