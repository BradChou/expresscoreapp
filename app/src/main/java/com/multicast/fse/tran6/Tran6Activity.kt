package com.multicast.fse.tran6

import android.R
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.text.Editable
import android.text.TextWatcher
import android.util.Base64.DEFAULT
import android.util.Log
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.FileProvider
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.multicast.fse.App
import com.multicast.fse.LoginActivity
import com.multicast.fse.RecyclerAdapter
import com.multicast.fse.databinding.ActivityTran6Binding
import com.multicast.fse.util.DialogUtil
import okhttp3.*
import okio.IOException
import org.json.JSONArray
import org.json.JSONObject
import java.io.ByteArrayOutputStream
import java.io.File
import java.util.*
import kotlin.math.sign


class Tran6Activity : AppCompatActivity() {

    companion object{
        var signPhoto: Bitmap? = null
    }

    private lateinit var binding:ActivityTran6Binding
    private lateinit var currentPhotoPath: String
    private val REQUEST_IMAGE_CAPTURE = 1
    private var paperPhoto :Bitmap? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityTran6Binding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)

        binding.editPackId.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                for(char in s!!){
                    if(char.code == 10){ //換行符號
                        getPackData(s.toString().trim())
                    }
                }
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            }
        })

        val layoutManager = LinearLayoutManager(this)
        layoutManager.orientation = LinearLayoutManager.VERTICAL

        binding.recyclerError.layoutManager = layoutManager
        binding.recyclerError.adapter = RecyclerAdapter.RecyclerErrorAdapter(arrayOf())
        binding.recyclerError.addItemDecoration(
            DividerItemDecoration(this,
                DividerItemDecoration.VERTICAL)
        )

        val layoutManager2 = LinearLayoutManager(this)
        layoutManager2.orientation = LinearLayoutManager.VERTICAL
        binding.recyclerUpload.layoutManager = layoutManager2
        binding.recyclerUpload.adapter = RecyclerAdapter.RecyclerUploadAdapter(arrayOf())
        binding.recyclerUpload.addItemDecoration(
            DividerItemDecoration(this, DividerItemDecoration.VERTICAL)
        )

        getItemCodeOptions()
    }

    override fun onResume() {
        super.onResume()
        if(signPhoto != null){
            sendStatus()
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {
            paperPhoto = BitmapFactory.decodeFile(currentPhotoPath)
            sendStatus()
        }
    }

    fun clickBack(view: View){
        this.finish()
    }

    fun clickSign(view:View){
        startActivity(Intent(this,Tran6SignActivity::class.java))
    }

    fun clickPaper(view: View){
        Intent(MediaStore.ACTION_IMAGE_CAPTURE).also { takePictureIntent ->
            takePictureIntent.resolveActivity(packageManager)?.also {
                // Create the File where the photo should go
                val photoFile: File? = try {
                    createImageFile()
                } catch (ex: IOException) {
                    // Error occurred while creating the File
                    null
                }
                // Continue only if the File was successfully created
                photoFile?.also {
                    val photoURI: Uri = FileProvider.getUriForFile(
                        this,
                        "$packageName.fileprovider",
                        it
                    )
                    takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI)
                    startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE)
                }
            }
        }
    }

    fun clickCheck(view:View){
        if(binding.editPackId.text.toString().isNotBlank()){
            getPackData(binding.editPackId.text.toString())
        }else{
            DialogUtil.showDailog(this,"請輸入貨號",getString(com.multicast.fse.R.string.sys_alert_title))
        }
    }

    fun clickUpload(view: View){
        sendStatus()
    }

    fun getPackData(packId:String){
        binding.editPackId.setText("")

        val url = App.SERVER +"/DeliveryMaster/GetPackData?PackId=$packId"

        val request = Request.Builder()
            .url(url)
            .build()

        Log.d("url",url)

        App.client.newCall(request).enqueue(object : Callback {
            override fun onFailure(call: Call, e: IOException) {
                e.printStackTrace()
            }

            override fun onResponse(call: Call, response: Response) {
                response.use {
                    val responseStr = it.body!!.string()
                    Log.d("getPackData",responseStr)
                    if (!it.isSuccessful) {
                        DialogUtil.showDailog(this@Tran6Activity,responseStr,getString(com.multicast.fse.R.string.sys_alert_title))
                    }
                    else{
                        val json = JSONObject(responseStr)

                        runOnUiThread {
                            if(json.getString("Status") == "200"){
                                if(json.getJSONObject("Data").getString("DeliveryId").isNotBlank()){
                                    (binding.recyclerUpload.adapter as RecyclerAdapter.RecyclerUploadAdapter).addItem(packId)
                                }
                                if(json.getJSONObject("Data").getString("CollectionMoney") != "0"){
                                    DialogUtil.showDailog(this@Tran6Activity,"代收貨款，收款金額：${json.getJSONObject("Data").getString("CollectionMoney")}","提醒")
                                }

                                if(binding.spinnerStatusDetail.selectedItem.toString().contains("正常配交")){
                                    binding.btnSign.isEnabled = true
                                    binding.btnPaper.isEnabled = true
                                }else{
                                    binding.btnSign.isEnabled = false
                                    binding.btnPaper.isEnabled = false
                                }
                                binding.btnUpload.isEnabled = true
                            }else{
                                DialogUtil.showDailog(this@Tran6Activity,json.getString("Message"),getString(
                                    com.multicast.fse.R.string.sys_alert_title))
                                if(json.getJSONObject("Data").getString("DeliveryId").isNotBlank()){
                                    (binding.recyclerError.adapter as RecyclerAdapter.RecyclerErrorAdapter).addItem(packId)
                                }
                            }
                            binding.txtDeliveryIdValue.text = json.getJSONObject("Data").getString("DeliveryId")
                            binding.txtQuantityValue.text = json.getJSONObject("Data").getString("Quantity")
                            binding.txtMoneyValue.text = json.getJSONObject("Data").getString("CollectionMoney")
                        }
                    }
                }
            }
        })
    }

    fun getItemCodeOptions(){
        val url = App.SERVER +"/DeliveryMaster/GetItemCodeOptions?CodeType=DeliveryStatus3"

        val request = Request.Builder()
            .url(url)
            .build()

        Log.d("url",url)

        App.client.newCall(request).enqueue(object : Callback {
            override fun onFailure(call: Call, e: IOException) {
                e.printStackTrace()
            }

            override fun onResponse(call: Call, response: Response) {
                response.use {
                    val responseStr = it.body!!.string()
                    Log.d("GetItemCodeOptions",responseStr)
                    if (!it.isSuccessful) {
                        DialogUtil.showDailog(this@Tran6Activity,responseStr,"title")
                    }
                    else{
                        val json = JSONObject(responseStr)
                        val data = arrayListOf<String>()
                        for(i in 0 until json.getJSONArray("Data").length()){
                            data.add(json.getJSONArray("Data").getJSONObject(i).getString("CodeId")+"."+json.getJSONArray("Data").getJSONObject(i).getString("CodeName"))
                        }

                        runOnUiThread {
                            val adapter1: ArrayAdapter<*> = ArrayAdapter(this@Tran6Activity,
                                R.layout.simple_spinner_dropdown_item,data)
                            binding.spinnerStatusDetail.adapter = adapter1
                            binding.spinnerStatusDetail.setSelection(data.indexOf(data.find { it.contains("正常配交") }))
                        }
                    }
                }
            }
        })
    }

    fun sendStatus(){
        binding.editPackId.setText("")

        if(signPhoto == null && paperPhoto == null && binding.spinnerStatusDetail.selectedItem.toString().contains("正常配交")){
            DialogUtil.showDailog(this,"請先簽名才能上傳貨態",getString(com.multicast.fse.R.string.sys_alert_title))
        }
        else{
            val url = App.SERVER +"/DeliveryMaster/SendStatus"

            var signBase64 = ""
            var paperBase64 = ""
            var inputFg = "APP"

            if(signPhoto != null){
                val byteArrayOutputStream = ByteArrayOutputStream()
                signPhoto!!.compress(Bitmap.CompressFormat.JPEG, 100, byteArrayOutputStream)
                var byteArray = byteArrayOutputStream.toByteArray()
                signBase64 = android.util.Base64.encodeToString(byteArray, DEFAULT)
                inputFg += "_SIGN"
            }
            if(paperPhoto != null){
                val byteArrayOutputStream = ByteArrayOutputStream()
                paperPhoto!!.compress(Bitmap.CompressFormat.JPEG,100,byteArrayOutputStream)
                var byteArray = byteArrayOutputStream.toByteArray()
                paperBase64 = android.util.Base64.encodeToString(byteArray, DEFAULT)
                inputFg += "_FULL"
            }

            val formBody = FormBody.Builder()
                .add("DeliveryId", binding.txtDeliveryIdValue.text.toString())
                .add("DeliveryStatus", "3")
                .add("DeliveryStatusDetail",binding.spinnerStatusDetail.selectedItem.toString().split(".")[0])
                .add("Input_FG",inputFg)
                .add("SignImage", signBase64)
                .add("SignImageFull",paperBase64)
                .add("JobUser",App.USER_ID)
                .add("JobDate",App.sdf.format(Date()))
                .build()

            val request = Request.Builder()
                .url(url)
                .header("Content-Type", "application/x-www-form-urlencoded")
                .post(formBody)
                .build()

            Log.d("url",url)

            App.client.newCall(request).enqueue(object : Callback {
                override fun onFailure(call: Call, e: IOException) {
                    e.printStackTrace()
                }

                override fun onResponse(call: Call, response: Response) {
                    response.use {
                        val responseStr = it.body!!.string()
                        Log.d("SendStatus",responseStr)
                        if (!it.isSuccessful) {
                            DialogUtil.showDailog(this@Tran6Activity,responseStr,"title")
                        }
                        else{
                            val json = JSONObject(responseStr)

                            runOnUiThread {
                                binding.txtDeliveryIdValue.text = ""
                                binding.txtQuantityValue.text = ""
                                binding.txtMoneyValue.text = ""
                                binding.btnSign.isEnabled = false
                                binding.btnPaper.isEnabled = false
                                binding.btnUpload.isEnabled = false
                                signPhoto = null
                                paperPhoto = null
                                if(json.getString("Status") == "200"){
                                    DialogUtil.showDailog(this@Tran6Activity,json.getString("Message"),getString(
                                        com.multicast.fse.R.string.sys_alert_title))
                                }else{
                                    DialogUtil.showDailog(this@Tran6Activity,json.getString("Message"),getString(
                                        com.multicast.fse.R.string.sys_alert_title))
                                }
                            }
                        }
                    }
                }
            })
        }
    }

    private fun createImageFile(): File {
        // Create an image file name
        val storageDir: File? = getExternalFilesDir(Environment.DIRECTORY_PICTURES)
        return File.createTempFile(
            "sign", /* prefix */
            ".jpg", /* suffix */
            storageDir /* directory */
        ).apply {
            // Save a file: path for use with ACTION_VIEW intents
            currentPhotoPath = absolutePath
        }
    }

}