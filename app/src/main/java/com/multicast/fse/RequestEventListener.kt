package com.multicast.fse

import android.util.Log
import com.multicast.fse.util.DialogUtil
import okhttp3.Call
import okhttp3.EventListener
import java.io.IOException

class RequestEventListener:EventListener() {

    override fun callStart(call: Call) {
        super.callStart(call)

        if(!call.request().url.toString().contains("Version/GetVersionInfo")){ //檢查版號不跳出訊息,防止dialog重疊關不掉
            if(call.request().url.toString().contains("Version/apk")){
                DialogUtil.showProgressDialog(App.currentActivity,"軟體更新，請稍後.....","正在下載...")
            }else{
                DialogUtil.showProgressDialog(App.currentActivity)
            }
        }
    }

    override fun callEnd(call: Call) {
        super.callEnd(call)
        if(!call.request().url.toString().contains("Version/GetVersionInfo")) {
            DialogUtil.dismissProgressDialog(App.currentActivity)
        }

    }

    override fun callFailed(call: Call, ioe: IOException) {
        super.callFailed(call, ioe)
        if(!call.request().url.toString().contains("Version/GetVersionInfo")) {
            DialogUtil.dismissProgressDialog(App.currentActivity)
            DialogUtil.showDailog(App.currentActivity,"連線失敗\n"+ioe.message,"系統通知")
        }
    }
}