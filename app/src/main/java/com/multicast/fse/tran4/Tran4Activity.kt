package com.multicast.fse.tran4

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.View
import android.widget.ArrayAdapter
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.zxing.Result
import com.multicast.fse.App
import com.multicast.fse.LoginActivity
import com.multicast.fse.R
import com.multicast.fse.RecyclerAdapter
import com.multicast.fse.databinding.ActivityTran4Binding
import com.multicast.fse.util.DialogUtil
import me.dm7.barcodescanner.zxing.ZXingScannerView
import okhttp3.*
import okio.IOException
import org.json.JSONObject
import java.util.*

class Tran4Activity : AppCompatActivity(), ZXingScannerView.ResultHandler {

    private lateinit var binding:ActivityTran4Binding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityTran4Binding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)

        with(binding.editPackId){
            addTextChangedListener(object : TextWatcher {
                override fun afterTextChanged(s: Editable?) {
                    for(char in s!!){
                        if(char.code == 10){ //換行符號
                            sendStatus(s.toString().trim())
                        }
                    }
                }

                override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
                }

                override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                }
            })
        }

        with(binding.recyclerError){
            val layoutManager = LinearLayoutManager(this@Tran4Activity)
            layoutManager.orientation = LinearLayoutManager.VERTICAL

            this.layoutManager = layoutManager
            adapter = RecyclerAdapter.RecyclerErrorAdapter(arrayOf())
            addItemDecoration(
                DividerItemDecoration(this@Tran4Activity,
                    DividerItemDecoration.VERTICAL)
            )
        }

        with(binding.recyclerUpload){
            val layoutManager = LinearLayoutManager(this@Tran4Activity)
            layoutManager.orientation = LinearLayoutManager.VERTICAL
            this.layoutManager = layoutManager
            adapter = RecyclerAdapter.RecyclerUploadAdapter(arrayOf())
            addItemDecoration(
                DividerItemDecoration(this@Tran4Activity,
                    DividerItemDecoration.VERTICAL)
            )
        }

        getStation()
        getItemCodeOptions()
    }

    override fun onDestroy() {
        super.onDestroy()
        binding.scannerView.stopCamera()
    }

    override fun handleResult(rawResult: Result?) {
        sendStatus(rawResult!!.text)
        openQRCamera()
    }

    private fun openQRCamera() {
        binding.scannerView.setResultHandler(this)
        binding.scannerView.startCamera()
    }

    fun clickScan(view: View){
        if(binding.scannerView.visibility == View.GONE){
            binding.scannerView.visibility = View.VISIBLE
            openQRCamera()
        }else{
            binding.scannerView.visibility = View.GONE
            binding.scannerView.stopCamera()
        }
    }

    fun clickUpload(view: View){
        if(binding.editPackId.text.isBlank()){
            DialogUtil.showDailog(this,"請輸入貨號",getString(R.string.sys_alert_title))
        }else{
            sendStatus(binding.editPackId.text.toString())
        }
    }

    fun clickBack(view: View){
        this.finish()
    }

    fun getStation(){
        val url = App.SERVER +"/DeliveryMaster/GetStation"

        val request = Request.Builder()
            .url(url)
            .header("accessToken", App.accessToken)
            .header("Cookie", App.headers["Set-Cookie"]!!) //必須給第一次cookie值，後端才知道是同一個session
            .get()
            .build()

        Log.d("url",url)

        App.client.newCall(request).enqueue(object : Callback {
            override fun onFailure(call: Call, e: IOException) {
                e.printStackTrace()
            }

            override fun onResponse(call: Call, response: Response) {
                response.use {
                    val responseStr = response.body!!.string()
                    Log.d("getStation",responseStr)
                    if (!response.isSuccessful) {
                        DialogUtil.showDailog(this@Tran4Activity,responseStr,getString(R.string.sys_alert_title))
                    }
                    else{
                        val json = JSONObject(responseStr)
                        val data = arrayListOf<String>()
                        for(i in 0 until json.getJSONArray("Data").length()){
                            data.add(json.getJSONArray("Data").getJSONObject(i).getString("DEPT_ID")+"."+json.getJSONArray("Data").getJSONObject(i).getString("DEPT_BRF"))
                        }

                        runOnUiThread {
                            val adapter1: ArrayAdapter<*> = ArrayAdapter(this@Tran4Activity,
                                android.R.layout.simple_spinner_dropdown_item,data)
                            binding.spinnerStation.adapter = adapter1

                            binding.spinnerStation.setSelection(data.indexOf(data.find { it.contains(
                                App.DEPART_ID) }))
                        }
                    }
                }
            }
        })
    }

    fun getItemCodeOptions(){
        val url = App.SERVER +"/DeliveryMaster/GetItemCodeOptions?CodeType=DeliveryStatus1"

        val request = Request.Builder()
            .url(url)
            .build()

        Log.d("url",url)

        App.client.newCall(request).enqueue(object : Callback {
            override fun onFailure(call: Call, e: IOException) {
                e.printStackTrace()
            }

            override fun onResponse(call: Call, response: Response) {
                response.use {
                    val responseStr = it.body!!.string()
                    Log.d("GetItemCodeOptions",responseStr)
                    if (!it.isSuccessful) {
                        DialogUtil.showDailog(this@Tran4Activity,responseStr,"title")
                    }
                    else{
                        val json = JSONObject(responseStr)
                        val data = arrayListOf<String>()
                        for(i in 0 until json.getJSONArray("Data").length()){
                            data.add(json.getJSONArray("Data").getJSONObject(i).getString("CodeId")+"."+json.getJSONArray("Data").getJSONObject(i).getString("CodeName"))
                        }

                        runOnUiThread {
                            val adapter1: ArrayAdapter<*> = ArrayAdapter(this@Tran4Activity,android.R.layout.simple_spinner_dropdown_item,data)
                            binding.spinnerStatusDetail.adapter = adapter1
                        }
                    }
                }
            }
        })
    }

    fun sendStatus(packId:String){
        binding.editPackId.setText("")
        if(!(binding.recyclerUpload.adapter as RecyclerAdapter.RecyclerUploadAdapter).isExist(packId)) {
            val url = App.SERVER + "/DeliveryMaster/SendStatus"

            val formBody = FormBody.Builder()
                .add("PackId", packId)
                .add("DeliveryStatus", "1")
                .add(
                    "DeliveryStatusDetail",
                    binding.spinnerStatusDetail.selectedItem.toString().split(".")[0]
                )
                .add("Input_FG", "APP")
                .add("JobUser", App.USER_ID)
                .add("JobDate", App.sdf.format(Date()))
                .add("StationCode", binding.spinnerStation.selectedItem.toString().split(".")[0])
                .build()
            val request = Request.Builder()
                .url(url)
                .header("Content-Type", "application/x-www-form-urlencoded")
                .post(formBody)
                .build()

            Log.d("url", url)

            App.client.newCall(request).enqueue(object : Callback {
                override fun onFailure(call: Call, e: IOException) {
                    e.printStackTrace()
                }

                override fun onResponse(call: Call, response: Response) {
                    response.use {
                        val responseStr = it.body!!.string()
                        Log.d("SendStatus", responseStr)
                        if (!it.isSuccessful) {
                            DialogUtil.showDailog(this@Tran4Activity, responseStr, "title")
                        } else {
                            val json = JSONObject(responseStr)

                            runOnUiThread {
                                if (json.getString("Status") == "200") {
                                    DialogUtil.showDailog(
                                        this@Tran4Activity,
                                        json.getString("Message"),
                                        getString(R.string.sys_alert_title)
                                    )
                                    if (json.getJSONObject("Data").getString("DeliveryId")
                                            .isNotBlank()
                                    ) {
                                        (binding.recyclerUpload.adapter as RecyclerAdapter.RecyclerUploadAdapter).addItem(
                                            packId
                                        )
                                    }
                                } else {
                                    DialogUtil.showDailog(
                                        this@Tran4Activity,
                                        json.getString("Message"),
                                        getString(R.string.sys_alert_title)
                                    )
                                    if (json.getJSONObject("Data").getString("DeliveryId")
                                            .isNotBlank()
                                    ) {
                                        (binding.recyclerError.adapter as RecyclerAdapter.RecyclerErrorAdapter).addItem(
                                            packId
                                        )
                                    }
                                }
                                binding.txtDeliveryIdValue.text =
                                    json.getJSONObject("Data").getString("DeliveryId")
                                binding.txtQuantityValue.text =
                                    json.getJSONObject("Data").getString("Quantity")
                            }
                        }
                    }
                }
            })
        }
    }
}