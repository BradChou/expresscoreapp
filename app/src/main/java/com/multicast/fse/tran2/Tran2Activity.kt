package com.multicast.fse.tran2

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import com.google.android.material.tabs.TabLayoutMediator
import com.multicast.fse.databinding.ActivityTran1Binding
import com.multicast.fse.databinding.ActivityTran2Binding
import com.multicast.fse.tran1.Tran1DetailPage

class Tran2Activity : AppCompatActivity() {

    private lateinit var binding:ActivityTran2Binding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityTran2Binding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)

        with(binding.viewPager2){
            val pageAdapter = Tran2Page.Tran2PagePageAdapter(this@Tran2Activity)
            adapter = pageAdapter
        }

        val title: ArrayList<String> = arrayListOf("條碼刷讀", "卸集清單")
        //綁定tabLayout與viewPager2
        TabLayoutMediator(binding.tabLayout2, binding.viewPager2) { tab, position ->
            if(position != title.size){
                tab.text = title[position]
            }
        }.attach()
    }

    fun clickBack(view: View){
        this.finish()
    }

}