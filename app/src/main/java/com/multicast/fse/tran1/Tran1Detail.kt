package com.multicast.fse.tran1

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.appcompat.content.res.AppCompatResources
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.google.android.material.tabs.TabLayoutMediator
import com.multicast.fse.R
import com.multicast.fse.databinding.ActivityTran1DetailBinding

class Tran1Detail : AppCompatActivity() {

    private lateinit var binding:ActivityTran1DetailBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityTran1DetailBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)

        with(binding.viewPager2){
            val pageAdapter = Tran1DetailPage.Tran1DetailPageAdapter(this@Tran1Detail,intent.getStringExtra("sendContact")!!)
            adapter = pageAdapter
        }

        val title: ArrayList<String> = arrayListOf("條碼刷讀", "集貨清單")
        //綁定tabLayout與viewPager2
        TabLayoutMediator(binding.tabLayout, binding.viewPager2) { tab, position ->
            if(position != title.size){
                tab.text = title[position]
            }
        }.attach()
    }

    fun clickBack(view: View){
        this.finish()
    }
}