package com.multicast.fse.tran1

import android.R.color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.CheckBox
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat.getColor
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.google.zxing.Result
import com.multicast.fse.App
import com.multicast.fse.R
import com.multicast.fse.RecyclerAdapter
import com.multicast.fse.databinding.FragmentTran1DetailBarcodeBinding
import com.multicast.fse.databinding.FragmentTran1DetailListBinding
import com.multicast.fse.util.DialogUtil
import kotlinx.coroutines.selects.select
import me.dm7.barcodescanner.zxing.ZXingScannerView
import okhttp3.*
import okio.IOException
import org.json.JSONArray
import org.json.JSONObject
import java.util.*
import kotlin.collections.ArrayList


class Tran1DetailPage {
    class Tran1DetailPageAdapter(activity: FragmentActivity,sendContact:String): FragmentStateAdapter(activity) {

        var fragments: ArrayList<Fragment> = arrayListOf(
            BarcodeFragment(sendContact),
            ListFragment(sendContact),
        )

        override fun getItemCount(): Int {
            return fragments.size
        }

        override fun createFragment(position: Int): Fragment {
            return fragments[position]
        }
    }

    //條碼刷讀畫面
    class BarcodeFragment(private val sendContact: String): Fragment(R.layout.fragment_tran1_detail_barcode), ZXingScannerView.ResultHandler {
        private var _binding: FragmentTran1DetailBarcodeBinding? = null
        // This property is only valid between onCreateView and onDestroyView.
        private val binding get() = _binding!!

        override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
        ): View? {
            _binding = FragmentTran1DetailBarcodeBinding.inflate(inflater, container, false)
            val view = binding.root
            getItemCodeOptions()

            with(binding.editPackId){
                addTextChangedListener(object : TextWatcher{
                    override fun afterTextChanged(s: Editable?) {
                        for(char in s!!){
                            if(char.code == 10){ //換行符號
                                sendStatus(s.toString().trim())
                            }
                        }
                    }

                    override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
                    }

                    override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                    }
                })
            }

            with(binding.btnScan){
                setOnClickListener {
                    if(binding.scannerView.visibility == View.GONE){
                        binding.scannerView.visibility = View.VISIBLE
                        openQRCamera()
                    }else{
                        binding.scannerView.visibility = View.GONE
                        binding.scannerView.stopCamera()
                    }
                }
            }

            with(binding.btnUpload){
                setOnClickListener {
                    if(binding.editPackId.text.isBlank()){
                        DialogUtil.showDailog(context,"請輸入貨號",getString(R.string.sys_alert_title))
                    }else{
                        sendStatus(binding.editPackId.text.toString())
                    }
                }
            }

            with(binding.recyclerError){
                val layoutManager = LinearLayoutManager(context)
                layoutManager.orientation = LinearLayoutManager.VERTICAL

                this.layoutManager = layoutManager
                adapter = RecyclerAdapter.RecyclerErrorAdapter(arrayOf())
                addItemDecoration(DividerItemDecoration(context,DividerItemDecoration.VERTICAL))
            }

           with(binding.recyclerUpload){
               val layoutManager = LinearLayoutManager(context)
               layoutManager.orientation = LinearLayoutManager.VERTICAL

               this.layoutManager = layoutManager
               adapter = RecyclerAdapter.RecyclerUploadAdapter(arrayOf())
               addItemDecoration(DividerItemDecoration(context,DividerItemDecoration.VERTICAL))
           }

            return view
        }

        override fun onDestroyView() {
            super.onDestroyView()
            binding.scannerView.stopCamera()
            _binding = null
        }

        override fun handleResult(rawResult: Result?) {
            sendStatus(rawResult!!.text)
            openQRCamera()
        }

        private fun openQRCamera() {
            binding.scannerView.setResultHandler(this)
            binding.scannerView.startCamera()
        }

        fun sendStatus(packId:String){
            binding.editPackId.setText("")
            //已成功貨號不再往後送
            if(!(binding.recyclerUpload.adapter as RecyclerAdapter.RecyclerUploadAdapter).isExist(packId)){
                val url = App.SERVER +"/DeliveryMaster/SendStatus"

                val formBody = FormBody.Builder()
                    .add("PackId", packId)
                    .add("DeliveryStatus", "5")
                    .add("DeliveryStatusDetail",binding.spinnerStatusDetail.selectedItem.toString().split(".")[0])
                    .add("Input_FG","APP")
                    .add("JobUser",App.USER_ID)
                    .add("JobDate",App.sdf.format(Date()))
                    .add("SendContact",sendContact)
                    .add("StationCode",App.DEPART_ID)
                    .build()
                val request = Request.Builder()
                    .url(url)
                    .header("Content-Type", "application/x-www-form-urlencoded")
                    .post(formBody)
                    .build()

                Log.d("url",url)

                App.client.newCall(request).enqueue(object : Callback {
                    override fun onFailure(call: Call, e: IOException) {
                        e.printStackTrace()
                    }

                    override fun onResponse(call: Call, response: Response) {
                        response.use {
                            val responseStr = it.body!!.string()
                            Log.d("SendStatus",responseStr)
                            if (!it.isSuccessful) {
                                DialogUtil.showDailog(context,responseStr,"title")
                            }
                            else{
                                val json = JSONObject(responseStr)

                                activity!!.runOnUiThread {
                                    if(json.getString("Status") == "200"){
                                        DialogUtil.showDailog(context,json.getString("Message"),getString(R.string.sys_alert_title))
                                        if(json.getJSONObject("Data").getString("DeliveryId").isNotBlank()){
                                            (binding.recyclerUpload.adapter as RecyclerAdapter.RecyclerUploadAdapter).addItem(packId)
                                        }
                                    }else{
                                        DialogUtil.showDailog(context,json.getString("Message"),getString(R.string.sys_alert_title))
                                        if(json.getJSONObject("Data").getString("DeliveryId").isNotBlank()){
                                            (binding.recyclerError.adapter as RecyclerAdapter.RecyclerErrorAdapter).addItem(packId)
                                        }
                                    }
                                    binding.txtDeliveryIdValue.text = json.getJSONObject("Data").getString("DeliveryId")
                                    binding.txtQuantityValue.text = json.getJSONObject("Data").getString("Quantity")
                                }
                            }
                        }
                    }
                })
            }
        }

        fun getItemCodeOptions(){
            val url = App.SERVER +"/DeliveryMaster/GetItemCodeOptions?CodeType=DeliveryStatus5"

            val request = Request.Builder()
                .url(url)
                .build()

            Log.d("url",url)

            App.client.newCall(request).enqueue(object : Callback {
                override fun onFailure(call: Call, e: IOException) {
                    e.printStackTrace()
                }

                override fun onResponse(call: Call, response: Response) {
                    response.use {
                        val responseStr = it.body!!.string()
                        Log.d("GetStatus5DetailList",responseStr)
                        if (!it.isSuccessful) {
                            DialogUtil.showDailog(context,responseStr,"title")
                        }
                        else{
                            val json = JSONObject(responseStr)
                            val data = arrayListOf<String>()
                            for(i in 0 until json.getJSONArray("Data").length()){
                                data.add(json.getJSONArray("Data").getJSONObject(i).getString("CodeId")+"."+json.getJSONArray("Data").getJSONObject(i).getString("CodeName"))
                            }

                            activity!!.runOnUiThread {
                                val adapter1: ArrayAdapter<*> = ArrayAdapter(context!!,android.R.layout.simple_spinner_dropdown_item,data)
                                binding.spinnerStatusDetail.adapter = adapter1
                                binding.spinnerStatusDetail.setSelection(data.indexOf(data.find{ it.contains("20") }))
                            }
                        }
                    }
                }
            })
        }
    }

    //集貨清單畫面
    class ListFragment(private val sendContact: String): Fragment(R.layout.fragment_tran1_detail_list){
        private var _binding: FragmentTran1DetailListBinding? = null
        // This property is only valid between onCreateView and onDestroyView.
        private val binding get() = _binding!!
        private var dataFlag = "A" //A:總資料;U:未完成資料

        override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
        ): View? {
            _binding = FragmentTran1DetailListBinding.inflate(inflater, container, false)
            val view = binding.root

            with(binding.txtSendContact){
                text = sendContact
            }
            with(binding.txtCountLabel){
                setOnClickListener {
                    if(dataFlag == "A"){
                        (binding.recycler.adapter as RecyclerAdapter).total()
                        dataFlag = "U"
                    }else{
                        (binding.recycler.adapter as RecyclerAdapter).undone()
                        dataFlag = "A"
                    }
                }
            }
            with(binding.txtCheck){
                setOnClickListener {
                    (binding.recycler.adapter as RecyclerAdapter).checkAll()
                }
            }
            with(binding.btnUpdate){
                setOnClickListener {
                    exectListStatus()
                }
            }

            getItemCodeOptions()
            return view
        }

        override fun onResume() {
            super.onResume()
            getDeliveryListTranDetail()
        }

        override fun onDestroyView() {
            super.onDestroyView()
            _binding = null
        }

        fun getItemCodeOptions(){
            val url = App.SERVER +"/DeliveryMaster/GetItemCodeOptions?CodeType=DeliveryStatus5"

            val request = Request.Builder()
                .url(url)
                .build()

            Log.d("url",url)

            App.client.newCall(request).enqueue(object : Callback {
                override fun onFailure(call: Call, e: IOException) {
                    e.printStackTrace()
                }

                override fun onResponse(call: Call, response: Response) {
                    response.use {
                        val responseStr = it.body!!.string()
                        Log.d("GetStatus5DetailList",responseStr)
                        if (!it.isSuccessful) {
                            DialogUtil.showDailog(context,responseStr,"title")
                        }
                        else{
                            val json = JSONObject(responseStr)
                            val data = arrayListOf<String>()
                            for(i in 0 until json.getJSONArray("Data").length()){
                                if(json.getJSONArray("Data").getJSONObject(i).getString("CodeId") != "20"){
                                    data.add(json.getJSONArray("Data").getJSONObject(i).getString("CodeId")+"."+json.getJSONArray("Data").getJSONObject(i).getString("CodeName"))
                                }
                            }

                            activity!!.runOnUiThread {
                                val adapter1: ArrayAdapter<*> = ArrayAdapter(context!!,android.R.layout.simple_spinner_dropdown_item,data)
                                binding.spinnerStatusDetail.adapter = adapter1
                            }
                        }
                    }
                }
            })
        }

        fun getDeliveryListTranDetail(){
            val url = App.SERVER +"/DeliveryMaster/DeliveryListTranDetail?SendContact=$sendContact&UserId=${App.USER_ID}"

            val request = Request.Builder()
                .url(url)
                .build()

            Log.d("url",url)

            App.client.newCall(request).enqueue(object : Callback {
                override fun onFailure(call: Call, e: IOException) {
                    e.printStackTrace()
                }

                override fun onResponse(call: Call, response: Response) {
                    response.use {
                        val responseStr = it.body!!.string()
                        Log.d("GetDeliveryListTranDetail",responseStr)
                        if (!it.isSuccessful) {
                            activity!!.runOnUiThread {
                                DialogUtil.showDailog(context,responseStr,getString(R.string.sys_alert_title))
                            }
                        }
                        else{
                            val json = JSONObject(responseStr)

                            activity!!.runOnUiThread {

                                var undone = 0
                                for(i in 0 until json.getJSONArray("Data").length()){
                                    if(json.getJSONArray("Data").getJSONObject(i).getString("Flag") == "未完成"){
                                        undone++
                                    }
                                }
                                binding.txtCountValue.text = "${undone}/${json.getJSONArray("Data").length()}"
                                val layoutManager = LinearLayoutManager(context)
                                layoutManager.orientation = LinearLayoutManager.VERTICAL
                                binding.recycler.layoutManager = layoutManager
                                binding.recycler.adapter = RecyclerAdapter(json.getJSONArray("Data"))
                                (binding.recycler.adapter as RecyclerAdapter).refreshView()
                                binding.recycler.addItemDecoration(
                                    DividerItemDecoration(context,
                                        DividerItemDecoration.VERTICAL)
                                )
                            }
                        }
                    }
                }
            })
        }

        fun exectListStatus(){
            val url = App.SERVER +"/DeliveryMaster/ExectListStatus"

            val formBody = FormBody.Builder()
                .add("DeliveryId", (binding.recycler.adapter as RecyclerAdapter).getSelectDataStr())
                .add("DeliveryStatus", "5")
                .add("DeliveryStatusDetail",binding.spinnerStatusDetail.selectedItem.toString().split(".")[0])
                .add("Input_FG","APP")
                .add("JobUser",App.USER_ID)
                .add("JobDate",App.sdf.format(Date()))
                .add("SendContact",sendContact)
                .add("StationCode",App.DEPART_ID)
                .build()
            val request = Request.Builder()
                .url(url)
                .header("Content-Type", "application/x-www-form-urlencoded")
                .post(formBody)
                .build()

            Log.d("url",url)

            App.client.newCall(request).enqueue(object : Callback {
                override fun onFailure(call: Call, e: IOException) {
                    e.printStackTrace()
                }

                override fun onResponse(call: Call, response: Response) {
                    response.use {
                        val responseStr = it.body!!.string()
                        Log.d("exectListStatus",responseStr)
                        if (!it.isSuccessful) {
                            DialogUtil.showDailog(context,responseStr,"title")
                        }
                        else{
                            val json = JSONObject(responseStr)

                            activity!!.runOnUiThread {
                                if(json.getString("Status") == "200"){
                                    DialogUtil.showDailog(context,json.getString("Message"),getString(R.string.sys_alert_title))
                                }else{
                                    DialogUtil.showDailog(context,json.getString("Message"),getString(R.string.sys_alert_title))
                                }
                                getDeliveryListTranDetail()
                            }
                        }
                    }
                }
            })
        }

        inner class RecyclerAdapter(private var dataSet:JSONArray) :
            RecyclerView.Adapter<RecyclerAdapter.ViewHolder>() {
            private val originData = dataSet.toString()
            private val selectData = arrayListOf<String>()
            private var isCheckAll = false
            /**
             * Provide a reference to the type of views that you are using
             * (custom ViewHolder).
             */
            inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
                val deliveryId: TextView
                val type:TextView
                val quantity:TextView
                val layout:ConstraintLayout
                val checkBox:CheckBox
                init {
                    // Define click listener for the ViewHolder's View.
                    deliveryId = view.findViewById(R.id.txt_deliveryId_value)
                    type = view.findViewById(R.id.txt_type_value)
                    quantity = view.findViewById(R.id.txt_quantity_value)
                    layout = view.findViewById(R.id.layout)
                    checkBox = view.findViewById(R.id.checkBox)

                    checkBox.setOnClickListener {
                        if(checkBox.isChecked){
                            selectData.add(deliveryId.text.toString())
                        }else{
                            selectData.remove(deliveryId.text.toString())
                        }
                        refreshView()
                    }
                }
            }

            // Create new views (invoked by the layout manager)
            override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): ViewHolder {
                // Create a new view, which defines the UI of the list item
                val view = LayoutInflater.from(viewGroup.context)
                    .inflate(R.layout.item_tran1_detail_list, viewGroup, false)

                return ViewHolder(view)
            }

            // Replace the contents of a view (invoked by the layout manager)
            override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {

                // Get element from your dataset at this position and replace the
                // contents of the view with that element
                val item = dataSet.getJSONObject(position)

                viewHolder.deliveryId.text = item.getString("DeliveryId")
                viewHolder.quantity.text = "${item.getString("Rest")}/${item.getString("Quantity")}"
                viewHolder.type.text = item.getString("Type")

                if(item.getString("Flag") == "已完成"){
                    viewHolder.layout.setBackgroundColor(getColor(context!!,R.color.gray))
                    viewHolder.checkBox.visibility = View.GONE
                }else if(item.getString("Flag") == "部分完成"){
                    viewHolder.layout.setBackgroundColor(getColor(context!!,R.color.orange))
                    viewHolder.checkBox.visibility = View.VISIBLE
                }else{
                    viewHolder.layout.setBackgroundColor(getColor(context!!,R.color.white))
                    viewHolder.checkBox.visibility = View.VISIBLE
                }

                viewHolder.checkBox.isChecked = isCheckAll
            }

            // Return the size of your dataset (invoked by the layout manager)
            override fun getItemCount() = dataSet.length()

            fun refreshView(){
                binding.txtSelectValue.text = selectData.size.toString()

                if(selectData.size > 0){
                    binding.txtStatusDetail.visibility = View.VISIBLE
                    binding.spinnerStatusDetail.visibility = View.VISIBLE
                    binding.btnUpdate.visibility = View.VISIBLE
                }else{
                    binding.txtStatusDetail.visibility = View.GONE
                    binding.spinnerStatusDetail.visibility = View.GONE
                    binding.btnUpdate.visibility = View.GONE
                }
            }

            fun undone(){
                var index = 0
                while (index<dataSet.length()){
                    if(dataSet.getJSONObject(index).getString("Flag") == "已完成"){
                        dataSet.remove(index)
                    }else{
                        index++
                    }
                }
                notifyDataSetChanged()
            }

            fun checkAll(){
                isCheckAll = true
                notifyDataSetChanged()
                selectData.clear()
                for(i in 0 until dataSet.length()){
                    if(dataSet.getJSONObject(i).getString("Flag") != "已完成"){
                        selectData.add(dataSet.getJSONObject(i).getString("DeliveryId"))
                    }
                }
                refreshView()
            }

            fun getSelectDataStr():String{
                return selectData.joinToString(separator = ",")
            }

            fun total(){
                dataSet = JSONArray(originData)
                notifyDataSetChanged()
            }
        }
    }
}