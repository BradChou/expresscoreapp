package com.multicast.fse.tran1

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.multicast.fse.App
import com.multicast.fse.LoginActivity
import com.multicast.fse.R
import com.multicast.fse.RecyclerAdapter
import com.multicast.fse.databinding.ActivityTran1Binding
import com.multicast.fse.util.DialogUtil
import okhttp3.*
import okio.IOException
import org.json.JSONArray
import org.json.JSONObject

class Tran1Activity : AppCompatActivity() {

    private lateinit var binding:ActivityTran1Binding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityTran1Binding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)

        with(binding.editSendName){
            addTextChangedListener(object : TextWatcher {
                override fun afterTextChanged(s: Editable?) {
                    for(char in s!!){
                        if(char.code == 10){ //換行符號
                            getPackData(s.toString().trim())
                        }
                    }
                }
                override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
                }
                override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                }
            })
        }

        getDeliveryListTran()
    }

    fun clickBack(view: View){
        this.finish()
    }

    fun clickSearch(view:View){
        if(binding.editSendName.text.isNotBlank()){
            getPackData(binding.editSendName.text.toString())
        }else{
            DialogUtil.showDailog(this,"請掃描貨號",getString(R.string.sys_alert_title))
        }
    }

    fun getDeliveryListTran(){
        val url = App.SERVER +"/DeliveryMaster/GetDeliveryListTran?userId=${App.USER_ID}&SendContact=${binding.editSendName.text}"


        val request = Request.Builder()
            .url(url)
            .build()

        Log.d("url",url)

        App.client.newCall(request).enqueue(object : Callback {
            override fun onFailure(call: Call, e: IOException) {
                e.printStackTrace()
            }

            override fun onResponse(call: Call, response: Response) {
                response.use {
                    val responseStr = it.body!!.string()
                    Log.d("getDeliveryList",responseStr)
                    if (!it.isSuccessful) {
                        runOnUiThread {
                            DialogUtil.showDailog(this@Tran1Activity,responseStr,"title")
                            //測試
                            val json = JSONArray()
                            json.put(JSONObject().put("SendContact","test").put("SendOriginalAddress","test").put("SendTel1","test")
                                .put("Quantity","test").put("UpdateDate","test"))

                                val layoutManager = LinearLayoutManager(this@Tran1Activity)
                                layoutManager.orientation = LinearLayoutManager.VERTICAL
                                binding.recycler.layoutManager = layoutManager
                                binding.recycler.adapter = RecyclerAdapter(json)
                                binding.recycler.addItemDecoration(DividerItemDecoration(this@Tran1Activity,DividerItemDecoration.VERTICAL))
                        }
                    }
                    else{
                        val json = JSONObject(responseStr)

                        runOnUiThread {
                            val layoutManager = LinearLayoutManager(this@Tran1Activity)
                            layoutManager.orientation = LinearLayoutManager.VERTICAL
                            binding.recycler.layoutManager = layoutManager
                            binding.recycler.adapter = RecyclerAdapter(json.getJSONArray("Data"))
                            binding.recycler.addItemDecoration(DividerItemDecoration(this@Tran1Activity,DividerItemDecoration.VERTICAL))
                        }
                    }
                }
            }
        })
    }

    fun getPackData(packId:String){

        val url = App.SERVER +"/DeliveryMaster/GetPackData?PackId=$packId"

        val request = Request.Builder()
            .url(url)
            .build()

        Log.d("url",url)

        App.client.newCall(request).enqueue(object : Callback {
            override fun onFailure(call: Call, e: IOException) {
                e.printStackTrace()
            }

            override fun onResponse(call: Call, response: Response) {
                response.use {
                    val responseStr = it.body!!.string()
                    Log.d("getPackData",responseStr)
                    if (!it.isSuccessful) {
                        DialogUtil.showDailog(this@Tran1Activity,responseStr,getString(com.multicast.fse.R.string.sys_alert_title))
                    }
                    else{
                        val json = JSONObject(responseStr)

                        runOnUiThread {
                            if(json.getString("Status") == "200"){
                                val i = Intent(this@Tran1Activity, Tran1Detail::class.java)
                                i.putExtra("sendContact",json.getJSONObject("Data").getString("SendContact"))
                                startActivity(i)
                            }else{
                                DialogUtil.showDailog(this@Tran1Activity,json.getString("Message"),getString(
                                    com.multicast.fse.R.string.sys_alert_title))
                            }
                        }
                    }
                }
            }
        })
    }

    inner class RecyclerAdapter(private val dataSet: JSONArray) :
        RecyclerView.Adapter<RecyclerAdapter.ViewHolder>() {

        /**
         * Provide a reference to the type of views that you are using
         * (custom ViewHolder).
         */
        inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
            val sendContact: TextView
            val address:TextView
            val tel:TextView
            val quantity:TextView
            val ticket:TextView
            val layout:ConstraintLayout
            init {
                // Define click listener for the ViewHolder's View.
                sendContact = view.findViewById(R.id.txt_sendContact_value)
                address = view.findViewById(R.id.txt_address_value)
                tel = view.findViewById(R.id.txt_tel_value)
                quantity = view.findViewById(R.id.txt_quantity_value)
                ticket = view.findViewById(R.id.txt_ticket_value)
                layout = view.findViewById(R.id.layout_main)

                layout.setOnClickListener {
                    val i = Intent(this@Tran1Activity, Tran1Detail::class.java)
                    i.putExtra("sendContact",sendContact.text)
                    startActivity(i)
                }
            }
        }

        // Create new views (invoked by the layout manager)
        override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): ViewHolder {
            // Create a new view, which defines the UI of the list item
            val view = LayoutInflater.from(viewGroup.context)
                .inflate(R.layout.item_tran1, viewGroup, false)

            return ViewHolder(view)
        }

        // Replace the contents of a view (invoked by the layout manager)
        override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {

            // Get element from your dataset at this position and replace the
            // contents of the view with that element
            val json = dataSet.getJSONObject(position)

            viewHolder.sendContact.text = json.getString("SendContact")
            viewHolder.address.text = json.getString("SendOriginalAddress")
            viewHolder.tel.text = json.getString("SendTel1")
            viewHolder.quantity.text = json.getString("Quantity")
            viewHolder.ticket.text = json.getString("Ticket")
        }

        // Return the size of your dataset (invoked by the layout manager)
        override fun getItemCount() = dataSet.length()

    }

}