package com.multicast.fse

import android.Manifest
import android.app.Activity
import android.app.AlertDialog
import android.app.Application
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.os.Environment
import android.util.Log
import android.view.LayoutInflater
import androidx.core.app.ActivityCompat
import androidx.core.content.FileProvider
import com.multicast.fse.util.DialogUtil
import okhttp3.*
import okio.BufferedSink
import okio.IOException
import okio.buffer
import okio.sink
import org.json.JSONArray
import org.json.JSONObject
import java.io.File
import java.text.SimpleDateFormat


class App: Application() {
    companion object  {
        var USER_ID: String = ""
        var USER_NAME: String = ""
        var ROLE_ID: String = ""
        var DEPART_ID:String = "" //登入者所屬站所
        var accessToken: String = ""
        val sdf = SimpleDateFormat("yyyy/MM/dd HH:mm:ss")

        lateinit var func: JSONArray
        lateinit var currentActivity:Activity

        val client = OkHttpClient.Builder()
            .eventListener(RequestEventListener())
            .build()
        lateinit var headers: Headers
        var SERVER:String = "http://www.logistics.org.tw/FSE" //API主機位置
        //var SERVER:String = "http://192.168.0.7:9004/FSE"
    }

    override fun onCreate() {
        super.onCreate()
        registerActivityLifecycleCallbacks(object :ActivityLifecycleCallbacks{
            override fun onActivityCreated(activity: Activity, savedInstanceState: Bundle?) {
                Log.d("create","create")
            }

            override fun onActivityStarted(activity: Activity) {
                Log.d("start","start")
            }

            override fun onActivityResumed(activity: Activity) {
                Log.d("resumed","resumed")
                currentActivity = activity //儲存目前開啟的Activity物件

                //檢查版號與安裝APK
                if (ActivityCompat.checkSelfPermission(activity, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED
                    && ActivityCompat.checkSelfPermission(activity, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED
                    && ActivityCompat.checkSelfPermission(activity, Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {

                    val url = "$SERVER/Version/GetVersionInfo"

                    val request = Request.Builder()
                        .url(url)
                        .get()
                        .build()

                    Log.d("url",url)

                    client.newCall(request).enqueue(object : Callback {
                        override fun onFailure(call: Call, e: IOException) {
                            e.printStackTrace()
                        }

                        override fun onResponse(call: Call, response: Response) {
                            response.use {
                                val responseStr = it.body!!.string()
                                if (!response.isSuccessful) {
                                    DialogUtil.showDailog(activity,responseStr,getString(R.string.sys_alert_title))
                                }
                                else{
                                    Log.d("getVersionInfo",responseStr)
                                    val json = JSONObject(responseStr)
                                    if(!getString(R.string.version).contains(json.getJSONObject("Data").getString("VERSION_NO"))){
                                        activity.runOnUiThread{
                                            val url = SERVER+"/Version/apk?fileName=${json.getJSONObject("Data").getString("FILE_NAME")}"
                                            Log.d("url",url)

                                            val request = Request.Builder()
                                                .url(url)
                                                .build()

                                            val client = OkHttpClient()

                                            val call  = client.newCall(request)

                                            call.enqueue(object : Callback {
                                                override fun onFailure(call: Call, e: java.io.IOException) {
                                                    e.printStackTrace()
                                                }
                                                override fun onResponse(call: Call, response: Response) {
                                                    try{
                                                        val downloadedFile = File(
                                                            Environment.getExternalStoragePublicDirectory(
                                                                Environment.DIRECTORY_DOWNLOADS), json.getJSONObject("Data").getString("FILE_NAME"))
                                                        val sink: BufferedSink = downloadedFile.sink().buffer()
                                                        sink.writeAll(response.body!!.source())
                                                        sink.close()

                                                        val uri = FileProvider.getUriForFile(activity, "$packageName.fileprovider", downloadedFile)
                                                        grantUriPermission(packageName,uri, Intent.FLAG_GRANT_READ_URI_PERMISSION)
                                                        val intent = Intent(Intent.ACTION_VIEW)
                                                        intent.flags = Intent.FLAG_GRANT_READ_URI_PERMISSION or Intent.FLAG_ACTIVITY_NEW_TASK
                                                        intent.putExtra(Intent.EXTRA_NOT_UNKNOWN_SOURCE,true)
                                                        intent.setDataAndType(uri, "application/vnd.android.package-archive")
                                                        startActivity(intent)
                                                    }catch (e:Exception){
                                                        Log.e("downloadAPK",e.message!!)
                                                        DialogUtil.showDailog(activity, "無法正常連線主機，請與資訊人員聯繫，稍後再嘗試登入", getString(R.string.sys_alert_title))
                                                    }
                                                }
                                            })
                                        }
                                    }
                                }
                            }
                        }
                    })

                }
            }

            override fun onActivityPaused(activity: Activity) {
                Log.d("paused","paused")
            }

            override fun onActivityStopped(activity: Activity) {
                Log.d("stopped","stopped")
            }

            override fun onActivitySaveInstanceState(activity: Activity, outState: Bundle) {
                Log.d("saveinstance","saveinstance")
            }

            override fun onActivityDestroyed(activity: Activity) {
                Log.d("destroyed","destroyed")
            }
        })
    }
}