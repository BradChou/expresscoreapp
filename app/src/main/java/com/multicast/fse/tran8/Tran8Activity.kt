package com.multicast.fse.tran8

import android.app.Dialog
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.zxing.Result
import com.multicast.fse.*
import com.multicast.fse.databinding.ActivityTran8Binding
import com.multicast.fse.util.DialogUtil
import me.dm7.barcodescanner.zxing.ZXingScannerView
import okhttp3.*
import okhttp3.MediaType.Companion.toMediaType
import okhttp3.RequestBody.Companion.toRequestBody
import okio.IOException
import org.json.JSONArray
import org.json.JSONObject
import java.util.*

class Tran8Activity : AppCompatActivity(), ZXingScannerView.ResultHandler {

    companion object{
        /*
        {
            "PackId":"",
            "StockLocation":"",
            "StationCode":"",
            "JobUser":""
        }
         */
        var uploadData = JSONArray()
    }

    private lateinit var binding:ActivityTran8Binding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityTran8Binding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)

        binding.txtUserValue.text = App.USER_NAME

        with(binding.editPackId){
            addTextChangedListener(object : TextWatcher {
                override fun afterTextChanged(s: Editable?) {
                    for(char in s!!){
                        if(char.code == 10){ //換行符號
                            binding.editPackId.setText(s.toString().trim())
                            addUploadData()
                        }
                    }
                }

                override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
                }

                override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                }
            })
        }

        with(binding.recyclerError){
            val layoutManager = LinearLayoutManager(this@Tran8Activity)
            layoutManager.orientation = LinearLayoutManager.VERTICAL

            this.layoutManager = layoutManager
            this.adapter = RecyclerAdapter.RecyclerErrorAdapter(arrayOf())
            this.addItemDecoration(
                DividerItemDecoration(this@Tran8Activity,
                    DividerItemDecoration.VERTICAL)
            )
        }

        with(binding.recyclerUpload){
            val layoutManager = LinearLayoutManager(this@Tran8Activity)
            layoutManager.orientation = LinearLayoutManager.VERTICAL

            this.layoutManager = layoutManager
            this.adapter = RecyclerAdapter.RecyclerUploadAdapter(arrayOf())
            this.addItemDecoration(
                DividerItemDecoration(this@Tran8Activity, DividerItemDecoration.VERTICAL)
            )
        }

        with(binding.spinnerLocation){
            val data = arrayListOf<Int>()
            for(i in 1..10){
                data.add(i)
            }
            val adapter1: ArrayAdapter<*> = ArrayAdapter(this@Tran8Activity,
                android.R.layout.simple_spinner_dropdown_item,data)
            adapter = adapter1
        }

        getStation()
    }

    override fun onDestroy() {
        super.onDestroy()
        binding.scannerView.stopCamera()
    }

    override fun handleResult(rawResult: Result?) {
        binding.editPackId.setText(rawResult!!.text)
        addUploadData()
        openQRCamera()
    }

    /**開啟QRCode相機 */
    private fun openQRCamera() {
        binding.scannerView.setResultHandler(this)
        binding.scannerView.startCamera()
    }

    fun clickBack(view: View){
        this.finish()
    }

    fun clickSave(view: View){
        addUploadData()
    }

    fun clickScan(view: View){
        if(binding.scannerView.visibility == View.GONE){
            binding.scannerView.visibility = View.VISIBLE
            openQRCamera()
        }else{
            binding.scannerView.visibility = View.GONE
            binding.scannerView.stopCamera()
        }
    }

    fun clickSearch(view: View){
        if(uploadData.length() > 0){
            startActivity(Intent(this, Tran8ListActivity::class.java))
        }else{
            DialogUtil.showDailog(this,"目前無資料",getString(R.string.sys_alert_title))
        }
    }

    fun clickUpload(view: View){
        if(uploadData.length()>0){
            uploadData()
        }else{
            DialogUtil.showDailog(this,"目前無資料",getString(R.string.sys_alert_title))
        }
    }

    fun addUploadData(){
        if(binding.editPackId.text.isNotBlank()){
            val item = JSONObject()
            item.put("PackId",binding.editPackId.text.toString())
                .put("StockLocation",binding.spinnerLocation.selectedItem.toString())
                .put("StationCode",binding.spinnerStation.selectedItem.toString().split(".")[0])
                .put("JobUser",binding.txtUserValue.text.toString())

            var isFind = false
            for(i in 0 until uploadData.length()){
                if(binding.editPackId.text.toString() == uploadData.getJSONObject(i).getString("PackId")){
                    isFind = true
                    uploadData.put(i,item)
                }
            }
            if(!isFind){
                uploadData.put(item)
            }

            binding.txtUploadValue.text = uploadData.length().toString()
            binding.editPackId.setText("")
        }else{
            DialogUtil.showDailog(this,"請輸入貨號",getString(R.string.sys_alert_title))
        }
    }

    fun getStation(){
        val url = App.SERVER +"/DeliveryMaster/GetStation"

        val request = Request.Builder()
            .url(url)
            .header("accessToken", App.accessToken)
            .header("Cookie", App.headers["Set-Cookie"]!!) //必須給第一次cookie值，後端才知道是同一個session
            .get()
            .build()

        Log.d("url",url)

        App.client.newCall(request).enqueue(object : Callback {
            override fun onFailure(call: Call, e: IOException) {
                e.printStackTrace()
            }

            override fun onResponse(call: Call, response: Response) {
                response.use {
                    val responseStr = response.body!!.string()
                    Log.d("getStation",responseStr)
                    if (!response.isSuccessful) {
                        DialogUtil.showDailog(this@Tran8Activity,responseStr,getString(R.string.sys_alert_title))
                    }
                    else{
                        val json = JSONObject(responseStr)
                        val data = arrayListOf<String>()
                        for(i in 0 until json.getJSONArray("Data").length()){
                            data.add(json.getJSONArray("Data").getJSONObject(i).getString("DEPT_ID")+"."+json.getJSONArray("Data").getJSONObject(i).getString("DEPT_BRF"))
                        }

                        runOnUiThread {
                            val adapter1: ArrayAdapter<*> = ArrayAdapter(this@Tran8Activity,
                                android.R.layout.simple_spinner_dropdown_item,data)
                            binding.spinnerStation.adapter = adapter1

                            binding.spinnerStation.setSelection(data.indexOf(data.find { it.contains(
                                App.DEPART_ID
                            ) }))
                        }
                    }
                }
            }
        })
    }

    fun uploadData(){
        val url = App.SERVER +"/DeliveryInStock/SaveData"

        val mediaType = "application/json; charset=utf-8".toMediaType()

        val requestBody: RequestBody = uploadData.toString().toRequestBody(mediaType)

        val request = Request.Builder()
            .url(url)
            .header("Content-Type", "application/json")
            .post(requestBody)
            .build()

        Log.d("url",url)

        App.client.newCall(request).enqueue(object : Callback {
            override fun onFailure(call: Call, e: IOException) {
                e.printStackTrace()
            }

            override fun onResponse(call: Call, response: Response) {
                response.use {
                    val responseStr = it.body!!.string()
                    Log.d("uploadData",responseStr)
                    if (!it.isSuccessful) {
                        DialogUtil.showDailog(this@Tran8Activity,responseStr,getString(R.string.sys_alert_title))
                    }
                    else{
                        val json = JSONObject(responseStr)

                        runOnUiThread {
                            if(json.getString("Status") == "200"){
                                DialogUtil.showDailog(this@Tran8Activity,json.getString("Message"),getString(
                                    com.multicast.fse.R.string.sys_alert_title))

                                for(i in 0 until uploadData.length()){
                                    (binding.recyclerUpload.adapter as RecyclerAdapter.RecyclerUploadAdapter).addItem(uploadData.getJSONObject(i).getString("PackId"))
                                }
                            }else{
                                DialogUtil.showDailog(this@Tran8Activity,json.getString("Message"),getString(
                                    com.multicast.fse.R.string.sys_alert_title))
                                for(i in 0 until uploadData.length()){
                                    (binding.recyclerError.adapter as RecyclerAdapter.RecyclerErrorAdapter).addItem(uploadData.getJSONObject(i).getString("PackId"))
                                }
                            }
                            uploadData = JSONArray()
                        }
                    }
                }
            }
        })
    }
}