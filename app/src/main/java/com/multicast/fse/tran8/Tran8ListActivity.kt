package com.multicast.fse.tran8

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.multicast.fse.R
import com.multicast.fse.databinding.ActivityTran8Binding
import com.multicast.fse.databinding.ActivityTran8ListBinding
import org.json.JSONArray
import org.json.JSONObject

class Tran8ListActivity : AppCompatActivity() {
    private lateinit var binding: ActivityTran8ListBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityTran8ListBinding.inflate(layoutInflater)
        setContentView(binding.root)

        with(binding.recycler){
            val data = JSONArray().put(JSONObject().put("PackId","貨號")
                .put("StockLocation","庫位")
                .put("StationCode","盤點站所")
                .put("JobUser","作業人員"))

            for (i in 0 until Tran8Activity.uploadData.length()){
                data.put(Tran8Activity.uploadData.getJSONObject(i))
            }

            val layoutManager = LinearLayoutManager(this@Tran8ListActivity)
            layoutManager.orientation = LinearLayoutManager.VERTICAL

            this.layoutManager = layoutManager
            this.adapter = RecyclerTran8Adapter(data)
            this.addItemDecoration(DividerItemDecoration(this@Tran8ListActivity, DividerItemDecoration.VERTICAL))
        }
    }

    fun clickBack(view: View){
        this.finish()
    }

    class RecyclerTran8Adapter(private var dataSet: JSONArray):
        RecyclerView.Adapter<RecyclerTran8Adapter.ViewHolder>(){
        /**
         * Provide a reference to the type of views that you are using
         * (custom ViewHolder).
         */
        inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
            val packId: TextView
            val location: TextView
            val station: TextView
            val user: TextView
            init {
                // Define click listener for the ViewHolder's View.
                packId = view.findViewById(R.id.txt_packId)
                location = view.findViewById(R.id.txt_location)
                station = view.findViewById(R.id.txt_station)
                user = view.findViewById(R.id.txt_user)
            }
        }

        // Create new views (invoked by the layout manager)
        override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): ViewHolder {
            // Create a new view, which defines the UI of the list item
            val view = LayoutInflater.from(viewGroup.context)
                .inflate(R.layout.item_tran8_list, viewGroup, false)

            return ViewHolder(view)
        }

        // Replace the contents of a view (invoked by the layout manager)
        override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {

            // Get element from your dataset at this position and replace the
            // contents of the view with that element
            val item = dataSet.getJSONObject(position)

            viewHolder.packId.text = item.getString("PackId")
            viewHolder.location.text = item.getString("StockLocation")
            viewHolder.station.text = item.getString("StationCode")
            viewHolder.user.text = item.getString("JobUser")
        }

        // Return the size of your dataset (invoked by the layout manager)
        override fun getItemCount() = dataSet.length()

    }
}