package com.multicast.fse

import android.Manifest
import android.app.Activity
import android.app.AlertDialog
import android.app.Dialog
import android.content.DialogInterface
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.os.Environment
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.FileProvider
import com.multicast.fse.databinding.ActivityLoginBinding
import com.multicast.fse.databinding.DialogAppInfoBinding
import com.multicast.fse.databinding.DialogServerBinding
import com.multicast.fse.util.DialogUtil
import okhttp3.*
import okio.BufferedSink
import okio.IOException
import okio.buffer
import okio.sink
import org.json.JSONArray
import org.json.JSONObject
import java.io.File
import java.text.SimpleDateFormat


class LoginActivity : AppCompatActivity() {


    private lateinit var binding: ActivityLoginBinding
    private var isPermission = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityLoginBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)

        getPermission()
    }

    fun getPermission(){
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED
            && ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED
            && ActivityCompat.checkSelfPermission(this,Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE,Manifest.permission.READ_EXTERNAL_STORAGE), 100)
        }else{
            isPermission = true
        }
    }

    /**取得權限回傳 */
    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String?>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == 100 && grantResults[0] == 0) {
            isPermission = true
        } else {
            Toast.makeText(this, "未取得權限無法使用此APP", Toast.LENGTH_SHORT).show()
        }
    }

    fun clickInfo(view:View){
        val dialog = Dialog(this)
        val bind :DialogAppInfoBinding = DialogAppInfoBinding.inflate(layoutInflater)
        dialog.setContentView(bind.root)
        dialog.show()
    }

    fun clickLogin(view: View){
        if(isPermission) {
            val url = App.SERVER + "/Login/Login"

            val formBody = FormBody.Builder()
                .add("USER_ID", binding.editUser.text.toString())
                .add("USER_PWD", binding.editPwd.text.toString())
                .build()
            val request = Request.Builder()
                .url(url)
                .header("Content-Type", "application/x-www-form-urlencoded")
                .post(formBody)
                .build()

            Log.d("url", url)

            App.client.newCall(request).enqueue(object : Callback {
                override fun onFailure(call: Call, e: IOException) {
                    e.printStackTrace()
                }

                override fun onResponse(call: Call, response: Response) {
                    response.use {
                        val responseStr = response.body!!.string()
                        runOnUiThread {
                            if (!response.isSuccessful) {
                                DialogUtil.showDailog(this@LoginActivity, responseStr, "title")
                            } else {
                                App.headers = response.headers
                                Log.d("LoginR", responseStr)

                                val json = JSONObject(responseStr)

                                App.USER_ID = json.getString("USER_ID")
                                App.USER_NAME = json.getString("USER_NAME")
                                App.ROLE_ID = json.getString("ROLE_ID")
                                App.DEPART_ID = json.getString("DEPART_ID")
                                App.accessToken = json.getString("token")
                                getAuth()
                            }
                        }
                    }
                }
            })
        }else{
            getPermission()
        }
    }

    fun clickServer(view:View){
        val builder = AlertDialog.Builder(this)
        val bind :DialogServerBinding = DialogServerBinding.inflate(layoutInflater)
        builder.setView(bind.root)
        builder.setTitle("輸入主機名稱")
        bind.editServer.setText(App.SERVER)
        builder.setPositiveButton("確定") { dialog, which ->
            App.SERVER = bind.editServer.text.toString()
        }
        builder.setNegativeButton("取消"){ dialog,which -> }
        val dialog = builder.create()
        dialog.show()
    }

    fun getAuth(){
        val url = App.SERVER+"/Home/Auth"

        val request = Request.Builder()
            .url(url)
            .header("accessToken", App.accessToken)
            .header("Cookie", App.headers["Set-Cookie"]!!) //必須給第一次cookie值，後端才知道是同一個session
            .get()
            .build()

        Log.d("url",url)

        App.client.newCall(request).enqueue(object : Callback {
            override fun onFailure(call: Call, e: IOException) {
                e.printStackTrace()
            }

            override fun onResponse(call: Call, response: Response) {
                response.use {
                    if (!response.isSuccessful) {
                        DialogUtil.showDailog(this@LoginActivity,response.body!!.string(),getString(R.string.sys_alert_title))
                    }
                    else{
                        val responseStr = response.body!!.string()
                        Log.d("getAuth",responseStr)
                        App.func = JSONArray(responseStr)
                        var index = 0
                        while(index<App.func.length()){
                            if(!App.func.getJSONObject(index).getString("FUNC_ID").contains("F")) {
                                App.func.remove(index)
                            }else{
                                index++
                            }
                        }
                        Log.d("func",App.func.toString())
                        startActivity(Intent(this@LoginActivity, MenuActivity::class.java))
                    }
                }
            }
        })
    }
}