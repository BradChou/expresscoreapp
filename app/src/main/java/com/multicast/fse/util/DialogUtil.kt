package com.multicast.fse.util

import android.app.Activity
import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import com.multicast.fse.R

class DialogUtil {
    companion object {
        private lateinit var progressDialog:AlertDialog
        private var progressCount = 0

        fun showDailog(context: Context?, msg: String, title: String) {
            var dialog: AlertDialog
            val builder =
                AlertDialog.Builder(context!!)
            builder.setMessage(msg)
                .setTitle(title)
            builder.setPositiveButton(
                "ok"
            ) { dialog, id ->
                dialog.dismiss()
            }
            builder.setCancelable(false)
            (context as Activity).runOnUiThread {
                dialog = builder.create()
                dialog.show()
            }
        }

        fun showProgressDialog(context: Context,title: String = "程序進行中，請稍候...",msg: String = ""){
            if(progressCount == 0) {
                val builder = AlertDialog.Builder(context)
                builder.setTitle(title)
                val v = LayoutInflater.from(context).inflate(R.layout.progress, null)
                v.findViewById<TextView>(R.id.txt_desc).text = msg
                builder.setView(v)
                //優化同時進來產生dialog的問題
                if(!this::progressDialog.isInitialized){
                    progressDialog = builder.create()
                    progressDialog.setCanceledOnTouchOutside(false)
                }else if(!progressDialog.isShowing){
                    progressDialog = builder.create()
                    progressDialog.setCanceledOnTouchOutside(false)
                }
                //end
                (context as Activity).runOnUiThread {
                    progressDialog.show()
                }
            }
            progressCount++
        }

        fun dismissProgressDialog(context: Context){
            progressCount--
            if(progressCount==0){
                (context as Activity).runOnUiThread {
                    progressDialog.dismiss()
                }
            }
        }
    }
}